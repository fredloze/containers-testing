# Containers-Testing

The purpose of this project is to deploy containers with different methods.
Containers should have volume using convoy driver, so the volumes can be snapshotted
and stored elsewere for backup usage.

## Tools for building

We are using docker_service module.
Before that we must copy the docker-compose.yml.

## Base config

The main VM we used for testing was built by vagrant following the guidelines
of the [coreos-save-testing project](https://gitlab.com/fredloze/coreos-save-testing/)

It is a simple coreos VM running inside virtualbox, provisionning with
ignition, python, pip, docker, docker-compose and jq.

## Users

Many Users can administrate the VM.
